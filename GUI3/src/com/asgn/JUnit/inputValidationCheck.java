package com.asgn.JUnit;

import com.asgn.Validation.Validate;

import junit.framework.TestCase;

public class inputValidationCheck extends TestCase {
	public Validate val = new Validate();

	// Test case id:1
	// Test case objective: To check valid value
	// Test case input:2
	// Test case Excepted output:true
	public void testAllValid001() {
		assertEquals(true, val.CheckInputDigit("2"));
	}

	// Test case id:2
	// Test case objective: To check Invalid Input
	// Test case input:2sfgs
	// Test case Excepted output:false
	public void testInput002() {
		assertEquals(false, val.CheckInputDigit("2sfgs"));
	}

	// Test case id:3
	// Test case objective: To check Empty Input
	// Test case input:""
	// Test case Excepted output:false
	public void testInput003() {
		assertEquals(false, val.CheckEmpty(""));
	}

	// Test case id:4
	// Test case objective: To check Celsius value less than -273.15 or Absolute
	// Zero
	// Test case input:-288
	// Test case Excepted output:false
	public void testInput004() {
		assertEquals(false, val.CheckCelLowerLimit("-288"));
	}

	// Test case id:5
	// Test case objective: To check Celsius value greater than -273.15 or Absolute
	// Zero
	// Test case input:-58
	// Test case Excepted output:true
	public void testInput005() {
		assertEquals(true, val.CheckCelLowerLimit("-58"));
	}

	// Test case id:6
	// Test case objective: To check Celsius value at boundary
	// Test case input:-273.15
	// Test case Excepted output:true
	public void testInput006() {
		assertEquals(true, val.CheckCelLowerLimit("-273.15"));
	}

	// Test case id:7
	// Test case objective: To check Celsius value greater than 100
	// Test case input:105
	// Test case Excepted output:false
	public void testInput007() {
		assertEquals(false, val.CheckCelUpperLimit("105"));
	}

	// Test case id:8
	// Test case objective: To check Celsius value less than 100
	// Test case input:88
	// Test case Excepted output:true
	public void testInput008() {
		assertEquals(true, val.CheckCelUpperLimit("88"));
	}

	// Test case id:9
	// Test case objective: To check Celsius value at boundary
	// Test case input:100
	// Test case Excepted output:true
	public void testInput009() {
		assertEquals(true, val.CheckCelUpperLimit("100"));
	}

	// Test case id:10
	// Test case objective: To check fahrenheit value less than -460 or Absolute
	// Zero
	// Test case input:-500
	// Test case Excepted output:false
	public void testInput010() {
		assertEquals(false, val.CheckFarLowerLimit("-500"));
	}

	// Test case id:11
	// Test case objective: To check fahrenheit value greater than -460 or Absolute
	// Zero
	// Test case input:-455
	// Test case Excepted output:true
	public void testInput011() {
		assertEquals(true, val.CheckFarLowerLimit("-455"));
	}

	// Test case id:12
	// Test case objective: To check fahrenheit value at boundary
	// Test case input:-460
	// Test case Excepted output:true
	public void testInput012() {
		assertEquals(true, val.CheckFarLowerLimit("-460"));
	}

	// Test case id:13
	// Test case objective: To check fahrenheit value greater than 212
	// Test case input:222
	// Test case Excepted output:false
	public void testInput013() {
		assertEquals(false, val.CheckFarUpperLimit("222"));
	}

	// Test case id:14
	// Test case objective: To check fahrenheit value less than 212
	// Test case input:200
	// Test case Excepted output:true
	public void testInput014() {
		assertEquals(true, val.CheckFarUpperLimit("200"));
	}

	// Test case id:15
	// Test case objective: To check fahrenheit value at boundary
	// Test case input:212
	// Test case Excepted output:true
	public void testInput015() {
		assertEquals(true, val.CheckFarUpperLimit("212"));
	}

	// Test case id:16
	// Test case objective: To check input
	// Test case input:-
	// Test case Excepted output:false
	public void testInput016() {
		assertEquals(false, val.CheckSign("-"));
	}

	// Test case id:17
	// Test case objective: To check input
	// Test case input:.
	// Test case Excepted output:false
	public void testInput017() {
		assertEquals(false, val.CheckSign("."));
	}

}
