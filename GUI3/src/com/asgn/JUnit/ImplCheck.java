package com.asgn.JUnit;

import com.asgn.Implement.Temp_converter;

import junit.framework.Assert;
import junit.framework.TestCase;

public class ImplCheck extends TestCase {
	
	Temp_converter cc = new Temp_converter();

	// Test case id:1
	// Test case objective: To check convertToFahrenheit function
	// Test case input:8
	// Test case Excepted output:
	String output = "46.4";
	public void testInput001() {
	double ans = cc.CovertToFahrenheit(8);
	String convert = ""+ans;
	Assert.assertEquals(output, convert);
	}

	// Test case id:2
	// Test case objective: To check convertToCelsius function
	// Test case input:8
	// Test case Excepted output:
	String output2 = "-13.333333333333332";
	public void testInput002() {
	double ans = cc.ConvertToCelsius(8);
	String convert = ""+ans;
	Assert.assertEquals(output2, convert);
	}

}
