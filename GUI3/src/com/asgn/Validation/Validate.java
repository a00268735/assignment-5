package com.asgn.Validation;

public class Validate {
	public boolean CheckInputDigit(String Input) {
		
		if (Input.matches("[-0-9.]+")){
			return true;			
	} 
		return false;
   }
	
	public boolean CheckEmpty(String Input) {
		if (Input.equals("")) {
			return false;
		}
		return true;
	}
	public boolean CheckSign(String Input) {
		if(Input.equals("-")||Input.equals(".")) {
			return false;
		}return true;
	}
	public boolean CheckCelLowerLimit(String Input) {
		double val = Double.parseDouble(Input);
		if(val < -273.15 ) {
			return false;
		}return true;
	}
	public boolean CheckCelUpperLimit(String Input) {
		double val = Double.parseDouble(Input);
		if(val > 100 ) {
			return false;
		}return true;
	}
	
	public boolean CheckFarLowerLimit(String Input) {
		double val = Double.parseDouble(Input);
		if(val < -460 ) {
			return false;
		}return true;
	}
	public boolean CheckFarUpperLimit(String Input) {
		double val = Double.parseDouble(Input);
		if(val > 212 ) {
			return false;
		}return true;
	}
}