package com.asgn.Implement;

import com.asgn.DAO.Temp_con_DAO;

public class Temp_converter implements Temp_con_DAO {

	@Override
	public double CovertToFahrenheit(double temp) {
        double fahrenheit = temp * 1.8 + 32;
        return fahrenheit;
		}

	@Override
	public double ConvertToCelsius(double temp) {
		double celsius = (temp - 32) / 1.8;
		return celsius;
	}

}
