package com.asgn.GUI;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;

import javax.swing.*;

import com.asgn.Data.Temp_con_Data;
import com.asgn.Implement.Temp_converter;
import com.asgn.Validation.Validate;
import com.sun.glass.events.MouseEvent;

public class MyGui3 extends JFrame {

	private static final int WIDTH = 400;
	private static final int HEIGHT = 120;
	private Validate val = new Validate();
	private Temp_converter impl = new Temp_converter();

	public MyGui3() {
		setProperties();
		init();
		setUI();
	}

	private JLabel label1;
	private JTextField text1, text2;
	private double celsius, fahrenheit;

	private void setProperties() {
		setSize(544, 419);
		setTitle("Temperature Converter");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	private void init() {

	}

	private void setUI() {
		getContentPane().setLayout(null);

		text1 = new JTextField();
		
		text1.setBounds(10, 85, 197, 40);
		getContentPane().add(text1);
		text2 = new JTextField();
		text2.setBounds(321, 85, 197, 39);
		getContentPane().add(text2);

		JButton btnNewButton = new JButton("Convert to Fahrenheit");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String temp_string = text1.getText();
				boolean flag = true;
				if (val.CheckEmpty(temp_string) == false) {
					JOptionPane.showMessageDialog(null, "Please enter a value!");
					flag = false;
				}
				else if (val.CheckSign(temp_string) == false) {
					JOptionPane.showMessageDialog(null, "Please enter a valid input!");
					flag = false;
				}
				else if (val.CheckInputDigit(temp_string) == false) {
					JOptionPane.showMessageDialog(null, "Please enter a valid input!");
					flag = false;
				}
				else if (val.CheckCelLowerLimit(temp_string) == false) {
					JOptionPane.showMessageDialog(null, "Entered value must not be lower than ABSOLUTE ZERO");
					flag = false;
				}
				else if (val.CheckCelUpperLimit(temp_string) == false) {
					JOptionPane.showMessageDialog(null, "Entered value must not be greater than 100 degrees");
					flag = false;
				}
				if (flag) {
					celsius = Double.parseDouble(temp_string);
					Temp_con_Data data = new Temp_con_Data();
					data.setCelsius(celsius);
					double result = impl.CovertToFahrenheit(data.getCelsius());
					text2.setText("" + result);
				}
			}
		});
		btnNewButton.setBackground(SystemColor.activeCaption);
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(172, 206, 189, 34);
		getContentPane().add(btnNewButton);

		JButton btnConvertToCelsius = new JButton("Convert to Celsius");
		btnConvertToCelsius.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String temp_string = text2.getText();
				boolean flag = true;
				if (val.CheckEmpty(temp_string) == false) {
					JOptionPane.showMessageDialog(null, "Please enter a value!");
					flag = false;
				}
				else if (val.CheckSign(temp_string) == false) {
					JOptionPane.showMessageDialog(null, "Please enter a valid input!");
					flag = false;
				}
				else if (val.CheckInputDigit(temp_string) == false) {
					JOptionPane.showMessageDialog(null, "Please enter a valid input!");
					flag = false;
				} 
				else if (val.CheckFarLowerLimit(temp_string) == false) {
					JOptionPane.showMessageDialog(null, "Entered value must not be lower than ABSOLUTE ZERO");
					flag = false;
				}
				else if (val.CheckFarUpperLimit(temp_string) == false) {
					JOptionPane.showMessageDialog(null, "Entered value must not be greater than 212 degrees");
					flag = false;
				}
				if (flag) {
					fahrenheit = Double.parseDouble(temp_string);
					Temp_con_Data data = new Temp_con_Data();
					data.setFahrenheit(fahrenheit);
					double result = impl.ConvertToCelsius(data.getFahrenheit());
					text1.setText("" + result);

				}
			}
		});
		btnConvertToCelsius.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnConvertToCelsius.setBackground(SystemColor.activeCaption);
		btnConvertToCelsius.setBounds(172, 268, 189, 34);
		getContentPane().add(btnConvertToCelsius);

		label1 = new JLabel("Celsius", SwingConstants.CENTER);
		label1.setForeground(SystemColor.window);
		label1.setOpaque(true);
		label1.setBackground(SystemColor.activeCaption);
		label1.setFont(new Font("Tahoma", Font.BOLD, 15));
		label1.setBounds(10, 114, 197, 34);
		getContentPane().add(label1);

		JLabel lblFahrenheit = new JLabel("Fahrenheit", SwingConstants.CENTER);
		lblFahrenheit.setOpaque(true);
		lblFahrenheit.setForeground(Color.WHITE);
		lblFahrenheit.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblFahrenheit.setBackground(SystemColor.activeCaption);
		lblFahrenheit.setBounds(321, 114, 197, 34);
		getContentPane().add(lblFahrenheit);

		JLabel label = new JLabel("=");
		label.setFont(new Font("Tahoma", Font.BOLD, 30));
		label.setBounds(254, 114, 46, 14);
		getContentPane().add(label);

		JLabel lblNewLabel = new JLabel("Formula");
		lblNewLabel.setBackground(Color.YELLOW);
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setOpaque(true);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel.setBounds(10, 159, 60, 22);
		getContentPane().add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("(\u00B0C \u00D7 9/5) + 32 = \u00B0F");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_1.setBounds(80, 164, 127, 14);
		getContentPane().add(lblNewLabel_1);
	}
}
